import socket
import select
import threading
import sys
import time
from PyQt5.QtWidgets import QApplication, QWidget, QDialog
from PyQt5.QtCore import pyqtSignal, QCoreApplication
from PyQt5 import uic
from PyQt5 import QtCore
from PyQt5.QtWidgets import QMessageBox


class MainForm(QWidget):

    updateList = pyqtSignal(str)
    addmsgsig = pyqtSignal(str)
    exitsignal = pyqtSignal()
    errsignal = pyqtSignal(str)

    def __init__(self, app, parent=None):
        QWidget.__init__(self, parent)
        self.app = app
        Form, Base = uic.loadUiType("chat.ui")
        self.mainform = Form()
        self.mainform.setupUi(self)
        self.pr = False
        self.errsignal.connect(self.showError)

        while 1:
            cw = ConnectForm(self)
            cw.setModal(True)
            cw.show()
            dres = cw.exec()
            if dres == QDialog.Rejected:
                exit()
            self.cs = ClientSocket(cw.getAddr(), 9000, self.updateList,
                                   self.addmsgsig, self.errsignal)
            try:
                self.cs.connect()
                break
            except ConnectionError as e:
                print('lol')
                self.errsignal.emit(str(e))

        self.updateList.connect(self.updateUserList)
        self.addmsgsig.connect(self.addMsg)
        self.mainform.sendButton.clicked.connect(self.sendmsg)
        self.mainform.msgEdit.returnPressed.connect(self.sendmsg)
        self.mainform.usrBox.itemDoubleClicked.connect(self.chooseDst)
        self.mainform.socketAddr.setText(cw.getAddr() + ':' + str(9000))
        self.username = cw.getNick()
        self.cs.sendmsg(('name:' + self.username), self.errsignal)
        time.sleep(0.1)
        self.cs.sendmsg('list', self.errsignal)
        self.dst = 'broadcast'

    def showError(self, text):
        mb = QMessageBox(self)
        mb.resize(400, 200)
        mb.setText('Connection Error: ' + text)
        mb.setModal(True)
        mb.exec()
        self.close()

    def sendmsg(self):

        msg = self.mainform.msgEdit.text()
        self.mainform.msgEdit.clear()
        head = ('p' if self.mainform.private_2.isChecked() else 'v') +\
               self.dst+':'+self.username+':'
        print(head+msg)
        self.cs.sendmsg(head+msg, self.errsignal)

    def updateUserList(self, answer):

        self.mainform.usrBox.clear()
        self.mainform.usrBox.addItem('Всім')
        for i in answer.split(' '):
            if i:
                self.mainform.usrBox.addItem(i)

    def addMsg(self, answer):

        print(answer)
        line = ''
        splitAnswer = answer.split(':')
        pr = splitAnswer[0][0]
        dst = splitAnswer[0][1:]
        src = splitAnswer[1]
        msg = ':'.join(splitAnswer[2:])
        if dst == 'broadcast':
            line = src + ': ' + msg
        else:
            line = src + ' -> ' + dst + ': ' + msg
        print(answer)
        if pr == 'p':
            line = '<p style="color:red"><i>' + line + '</i></p>'

        self.mainform.msgBox.append(line)

    def chooseDst(self, dst):

        dst = dst.text()
        if dst == 'Всім':
            self.dst = 'broadcast'
        else:
            self.dst = dst
        self.mainform.dstLabel.setText(dst+'->')

    def closeEvent(self, event):
        self.cs.sendmsg('disconnect', self.errsignal)
        event.accept()
        # print('kek')
        # self.exitsignal.emit()
        # sys.exit()


class ConnectForm(QDialog):

    def __init__(self, parent):
        QDialog.__init__(self, parent)
        self.setWindowFlags(QtCore.Qt.Dialog | QtCore.Qt.WindowSystemMenuHint)
        self.setWindowModality(QtCore.Qt.WindowModal)
        Form, Base = uic.loadUiType("serverInfo.ui")
        self.parent = parent
        self.form = Form()
        self.form.setupUi(self)

    def getAddr(self):
        return self.form.addr.text()

    def getNick(self):
        return self.form.lineEdit.text()


class ClientSocket:

    def __init__(self, addr, port, updsignal, addsignal, errSig):
        self.port = port
        self.addr = addr
        self.upds = updsignal
        self.adds = addsignal
        self.errSig = errSig

    def connect(self):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            self.sock.connect((self.addr, self.port))
        except:
            print('kek')
            raise ConnectionError('Server unreachable')

        # if self.sock.connect_ex((self.addr, self.port)):
        #     raise ConnectionError('Server broke connection')

        select.select([self.sock], [self.sock], [])
        self.rmess = ReadMessage(self.sock, self.upds, self.adds, self.errSig)
        self.rmess.start()

    def sendmsg(self, msg, signal):
        try:
            self.sock.send(msg.encode('utf-8'))
        except ConnectionResetError as e:
            signal.emit(str(e))


class ReadMessage(threading.Thread):

    def __init__(self, sock, updUsrSignal, updMsgSignal, errSig):
        threading.Thread.__init__(self)
        self.sock = sock
        self.updsig = updUsrSignal
        self.updmsgsig = updMsgSignal
        self.errSig = errSig

    def run(self):
        while 1:
            try:
                answer = self.sock.recv(1024).decode('utf-8')
            except ConnectionResetError as e:
                self.errSig.emit(str(e))
                break
            if answer.split(' ')[0] == 'list:':
                self.updsig.emit(answer[6:])
                continue
            self.updmsgsig.emit(answer)


if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = MainForm(app)
    window.show()
    qAppExe = app.exec()
    sys.exit(qAppExe)