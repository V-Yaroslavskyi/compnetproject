import socket
import threading


class ServerChat(threading.Thread):

    def __init__(self, ma, clientsocket):
        threading.Thread.__init__(self)
        self.map = ma
        self.clientsock = clientsocket
        self.users = []

    def sendList(self):
        output = 'list: '
        for user in self.users:
            output += user + ' '
        for user in self.users:
            self.map[user].send(output.encode('utf-8'))

    def run(self):

        while 1:
            message = self.clientsock.recv(1024).decode('utf-8')
            print(message)
            m = message.split(':')
            name = ''
            if m[0] == 'name':
                name = m[1]
                if name not in self.map.keys():
                    self.map[name] = self.clientsock
                    break
                else:
                    self.clientsock.send('nae'.encode('utf-8'))
            else:
                self.clientsock.send('you are not logged in'.encode('utf-8'))

        while 1:
            recvMessage = self.clientsock.recv(1024).decode('utf-8')
            splitmessage = recvMessage.split(':')
            print(recvMessage)
            self.users = self.map.keys()
            head = splitmessage[0]

            if head not in ['list', 'disconnect']:
                pr = head[0]
                head = head[1:]
                src = splitmessage[1]
                msg = ':'.join(splitmessage[2:])

            if head == 'list':
                self.sendList()

            elif (head in self.users) and (pr == 'p'):
                sockForSend = self.map[head]
                sockForSend.send(recvMessage.encode('utf-8'))
                sockForSend = self.map[src]
                sockForSend.send(recvMessage.encode('utf-8'))

            elif head == 'disconnect':
                self.map = self.map.pop(name)

            elif (head == 'broadcast') or (pr == 'v'):
                for i in self.users:
                    self.map[i].send(recvMessage.encode('utf-8'))


serversock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
serversock.bind(('0.0.0.0', 9000))
serversock.listen(5)

mymap = {}

while 1:
    clientsock, addr = serversock.accept()
    th = ServerChat(mymap, clientsock)
    th.start()